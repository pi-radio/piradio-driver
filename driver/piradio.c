#include <linux/kernel.h>
#include <linux/jiffies.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/string.h>
#include <linux/socket.h>
#include <linux/errno.h>
#include <linux/fcntl.h>
#include <linux/in.h>

#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/platform_device.h>
#include <linux/inet.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/skbuff.h>
#include <linux/ethtool.h>
#include <net/sock.h>
#include <net/checksum.h>
#include <linux/if_ether.h> /* For the statistics structure. */
#include <linux/if_arp.h> /* For ARPHRD_ETHER */s
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/percpu.h>
#include <linux/net_tstamp.h>
#include <net/net_namespace.h>
#include <linux/u64_stats_sync.h>
#include <linux/delay.h>
#include <linux/types.h>
#include <linux/random.h>
#include <net/inet_ecn.h>
#include "piradio.h"

#define NUM_BDS 3

#define TAP_SIZE 4
#define TX_DATA 32768
#define OFDM_SYMBOL_SIZE 80
#define OFDM_FRAME_SIZE (9 * OFDM_SYMBOL_SIZE)
#define MAX_BUF_SIZE OFDM_FRAME_SIZE
#define NOISE_EST_DATA_LEN OFDM_FRAME_SIZE
#define MAX_WAIT_Q_LEN 1
#define MAX_SENT_Q_LEN 1
#define MAX_PENDING_Q_LEN 128

struct sk_buff_head sent_queue0;
struct sk_buff_head wait_queue0;
struct sk_buff_head pending_queue0;

struct sk_buff_head sent_queue1;
struct sk_buff_head wait_queue1;
struct sk_buff_head pending_queue1;

static DECLARE_WAIT_QUEUE_HEAD(tx0_waitqueue);
static DECLARE_WAIT_QUEUE_HEAD(tx1_waitqueue);

struct hrtimer timer;
spinlock_t skb_lock;

static __u8 *ack_buffer0;
static __u8 *ack_buffer1;

MODULE_LICENSE("Dual BSD/GPL");

struct piradio_priv piradio_p0;
struct piradio_priv piradio_p1;
int curr_desc = 0;
struct sk_buff_head wait0;

static void remap_devs(void)
{
	piradio_p0.modul_base = ioremap(XIL_MODULATOR0, 256);
//	piradio_p0.fir_filt_base = ioremap(XIL_FIR_FILTER0, 256);
	piradio_p0.chann_emm_base = ioremap(XIL_CHANN_EMULATOR0, 256);
	piradio_p0.csma_delay_base = ioremap(XIL_CSMA_DELAY0, 256);
	piradio_p0.csma_status_base = ioremap(XIL_CSMA_STATUS0, 256);
	piradio_p0.sync_thresh_base = ioremap(XIL_SYNC_THRESH0, 128);

	piradio_p1.modul_base = ioremap(XIL_MODULATOR1, 256);
//	piradio_p1.fir_filt_base = ioremap(XIL_FIR_FILTER1, 256);
	piradio_p1.chann_emm_base = ioremap(XIL_CHANN_EMULATOR1, 256);
	piradio_p1.csma_delay_base = ioremap(XIL_CSMA_DELAY1, 256);
	piradio_p1.csma_status_base = ioremap(XIL_CSMA_STATUS1, 256);
	piradio_p1.sync_thresh_base = ioremap(XIL_SYNC_THRESH1, 128);

	reset_base = ioremap(XIL_RESET, 128);
}

static void unmap_devs(void)
{
	iounmap(piradio_p0.modul_base);
//	iounmap(piradio_p0.fir_filt_base);
	iounmap(piradio_p0.chann_emm_base);
	iounmap(piradio_p0.csma_delay_base);
	iounmap(piradio_p0.csma_status_base);
	iounmap(piradio_p0.sync_thresh_base);

	iounmap(piradio_p1.modul_base);
//	iounmap(piradio_p1.fir_filt_base);
	iounmap(piradio_p1.chann_emm_base);
	iounmap(piradio_p1.csma_delay_base);
	iounmap(piradio_p1.csma_status_base);
	iounmap(piradio_p1.sync_thresh_base);
}

static void write_reg(__u32 value, void __iomem *base)
{
	writel(value, base);
}

static __u32 read_reg(void __iomem *base)
{
	return readl(base);
}

static int config_dma(struct net_device *netdev, unsigned char *data,
		      unsigned int len, dma_addr_t *mapping,
		      enum dma_data_direction dir,
		      struct dma_async_tx_descriptor *chan_desc,
		      unsigned long dma_flags, struct dma_chan *chann,
		      struct device *dev, struct completion *comp,
		      dma_cookie_t *cookie, dma_async_tx_callback callback)
{
	struct piradio_hdr *pir = (struct piradio_hdr *)data;
	*mapping = dma_map_single(dev, data, len, dir);
	if (dma_mapping_error(dev, *mapping)) {
		printk(KERN_ERR "dma mapping error\n");
		goto error;
	}
	if (len != OFDM_FRAME_SIZE)
		printk(KERN_ERR "total length =  %d\n", len);
	chan_desc = dmaengine_prep_slave_single(chann, *mapping, len,
						DMA_MEM_TO_DEV, dma_flags);
	if (!chan_desc) {
		printk(KERN_ERR "dmaengine_prep*() error\n");
		goto error;
	}
	chan_desc->callback = callback;
	chan_desc->callback_param = comp;
	init_completion(comp);

	*cookie = dmaengine_submit(chan_desc);

	if (dma_submit_error(*cookie)) {
		printk("Submit error\n");
		goto error;
	}

	dma_async_issue_pending(chann);

	return 0;
error:
	dma_unmap_single(dev, data, len, dir);
	return -1;
}

static inline void frame_acked0(void)
{
	struct sk_buff *tmp;
	unsigned long l_flags;
	spin_lock_irqsave(&piradio_p0.timer_lock, l_flags);
	tmp = skb_dequeue(&wait_queue0);

	dev_consume_skb_any(tmp);
	if (!skb_queue_empty(&pending_queue0)) {
		tmp = skb_dequeue(&pending_queue0);
		skb_queue_head(&wait_queue0, tmp);
		hrtimer_start(&piradio_p0.csma_timer, 0, HRTIMER_MODE_REL);

		if (netif_queue_stopped(piradio_p0.netdev)) {
			netif_start_queue(piradio_p0.netdev);
		}
	}
	spin_unlock_irqrestore(&piradio_p0.timer_lock, l_flags);
}

static inline void frame_acked1(void)
{
	struct sk_buff *tmp;
	unsigned long l_flags;
	spin_lock_irqsave(&piradio_p1.timer_lock, l_flags);
	tmp = skb_dequeue(&wait_queue1);

	dev_consume_skb_any(tmp);
	if (!skb_queue_empty(&pending_queue1)) {
		tmp = skb_dequeue(&pending_queue1);
		skb_queue_head(&wait_queue1, tmp);
		hrtimer_start(&piradio_p1.csma_timer, 0, HRTIMER_MODE_REL);

		if (netif_queue_stopped(piradio_p1.netdev)) {
			netif_start_queue(piradio_p1.netdev);
		}
	}
	spin_unlock_irqrestore(&piradio_p1.timer_lock, l_flags);
}

static void tx_cmplt_callback0(void *completion)
{
	piradio_p0.link_stats.tx_packets++;
	piradio_p0.link_stats.tx_bytes += OFDM_FRAME_SIZE;
	if (piradio_p0.ack_enabled == 0) {
		//printk(KERN_ALERT "tx_cmplt_callback0 0");
		frame_acked0();
	} else {
		//printk(KERN_ALERT "tx_cmplt_callback0 1");
		hrtimer_start(&piradio_p0.ack_timer,
			      ktime_set(0, PIRADIO_ACK_HOLDOFF),
			      HRTIMER_MODE_REL);
	}
	complete(completion);
}

static void tx_cmplt_callback1(void *completion)
{
	piradio_p1.link_stats.tx_packets++;
	piradio_p1.link_stats.tx_bytes += OFDM_FRAME_SIZE;
	if (piradio_p1.ack_enabled == 0) {
		//printk(KERN_ALERT "tx_cmplt_callback1 0");
		frame_acked1();
	} else {
		//printk(KERN_ALERT "tx_cmplt_callback1 1");
		hrtimer_start(&piradio_p1.ack_timer,
			      ktime_set(0, PIRADIO_ACK_HOLDOFF),
			      HRTIMER_MODE_REL);
	}
	complete(completion);
}

static void tx_cmplt_callback_ack0(void *completion)
{
	piradio_p0.link_stats.tx_errors++;
	//printk(KERN_ALERT "ack tx callback 0");
	complete(completion);
}

static void tx_cmplt_callback_ack1(void *completion)
{
	piradio_p1.link_stats.tx_errors++;
	//printk(KERN_ALERT "ack tx callback 1");
	complete(completion);
}

static inline void ack_frame0(__u32 seq_num)
{
	struct piradio_hdr *pir = (struct piradio_hdr *)ack_buffer0;
	dma_addr_t mapping;
	unsigned long l_flags;
	__u16 csum;
	enum dma_ctrl_flags flags = DMA_CTRL_ACK | DMA_PREP_INTERRUPT;
	struct dma_async_tx_descriptor *chan_desc;
	int ret;
	pir->p_type = PIR_T_ACK;
	pir->p_seq_num = htonl(seq_num);
	csum = ip_compute_csum(ack_buffer0, OFDM_FRAME_SIZE - 2);
	memcpy(ack_buffer0 + OFDM_FRAME_SIZE - 2, &csum, 2);
	spin_lock_irqsave(&piradio_p0.lock, l_flags);
	ret = config_dma(piradio_p0.netdev, ack_buffer0, OFDM_FRAME_SIZE,
			 &mapping, DMA_MEM_TO_DEV, chan_desc, flags,
			 piradio_p0.tx_dma.channel_p, &piradio_p0.pdev->dev,
			 &piradio_p0.tx_dma.cmp, &piradio_p0.tx_dma.cookie,
			 &tx_cmplt_callback_ack0);
	if (ret < 0) {
		printk(KERN_ALERT "KERN ALEEEEERTTTTTTTTTTTTTTTTTTTTT");
	}
	dma_unmap_single(&piradio_p0.pdev->dev, mapping, MAX_BUF_SIZE,
			 DMA_DEV_TO_MEM);
	//printk(KERN_ALERT "p0 Sent ACK");
	spin_unlock_irqrestore(&piradio_p0.lock, l_flags);
}

static inline void ack_frame1(__u32 seq_num)
{
	struct piradio_hdr *pir = (struct piradio_hdr *)ack_buffer1;
	dma_addr_t mapping;
	unsigned long l_flags;
	enum dma_ctrl_flags flags = DMA_CTRL_ACK | DMA_PREP_INTERRUPT;
	struct dma_async_tx_descriptor *chan_desc;
	int ret;
	__u16 csum;
	pir->p_type = PIR_T_ACK;
	pir->p_seq_num = htonl(seq_num);
	csum = ip_compute_csum(ack_buffer1, OFDM_FRAME_SIZE - 2);
	memcpy(ack_buffer1 + OFDM_FRAME_SIZE - 2, &csum, 2);
	//printk(KERN_ALERT "p1 Sent ACK w csum = %llu", csum);
	spin_lock_irqsave(&piradio_p1.lock, l_flags);
	ret = config_dma(piradio_p1.netdev, ack_buffer1, OFDM_FRAME_SIZE,
			 &mapping, DMA_MEM_TO_DEV, chan_desc, flags,
			 piradio_p1.tx_dma.channel_p, &piradio_p1.pdev->dev,
			 &piradio_p1.tx_dma.cmp, &piradio_p1.tx_dma.cookie,
			 &tx_cmplt_callback_ack1);
	if (ret < 0) {
		printk(KERN_ALERT "KERN ALEEEEERTTTTTTTTTTTTTTTTTTTTT");
	}
	dma_unmap_single(&piradio_p1.pdev->dev, mapping, MAX_BUF_SIZE,
			 DMA_DEV_TO_MEM);

	//printk(KERN_ALERT "p1 Sent ACK");
	spin_unlock_irqrestore(&piradio_p1.lock, l_flags);
}

static int setup_rx(struct piradio_priv *piradio_p)
{
	enum dma_ctrl_flags flags = DMA_CTRL_ACK | DMA_PREP_INTERRUPT;
	dma_async_tx_callback callback;
	dma_addr_t mapping;
	unsigned long timeout = msecs_to_jiffies(3000);
	enum dma_status status;
	unsigned long l_flags;
	struct dma_async_tx_descriptor *chan_desc;

	if (piradio_p == &piradio_p0) {
		callback = &rx_cmplt_callback0;

	} else if (piradio_p == &piradio_p1) {
		callback = &rx_cmplt_callback1;
	}
	//spin_lock_irqsave(&piradio_p->lock, l_flags);
	mapping = dma_map_single(&piradio_p->pdev->dev,
				 piradio_p->rx_dma.rx_buffer, MAX_BUF_SIZE,
				 DMA_DEV_TO_MEM);
	if (dma_mapping_error(&piradio_p->pdev->dev, mapping)) {
		printk(KERN_ERR "RX dma mapping error\n");
		goto error2;
	}
	piradio_p->rx_dma.mapping = mapping;

	chan_desc = dmaengine_prep_slave_single(piradio_p->rx_dma.channel_p,
						mapping, MAX_BUF_SIZE,
						DMA_DEV_TO_MEM, flags);
	if (!chan_desc) {
		printk(KERN_ERR "RX dmaengine_prep*() error\n");
		goto error2;
	}
	chan_desc->callback = callback;
	chan_desc->callback_param = &piradio_p->rx_dma.cmp;
	init_completion(&piradio_p->rx_dma.cmp);

	piradio_p->rx_dma.cookie = dmaengine_submit(chan_desc);

	if (dma_submit_error(piradio_p->rx_dma.cookie)) {
		printk("RX Submit error\n");
		goto error2;
	}
	dma_async_issue_pending(piradio_p->rx_dma.channel_p);
	//spin_unlock_irqrestore(&piradio_p->lock, l_flags);
	return 0;
error2:
	//spin_unlock_irqrestore(&piradio_p->lock, l_flags);
	return -1;
}

static void rx_cmplt_callback0(void *completion)
{
	struct piradio_hdr *pir;
	struct piradio_hdr *pir_tmp;
	struct iphdr *ip_header;
	unsigned long l_flags;
	int ret;
	struct dma_async_tx_descriptor *chan_desc;
	enum dma_ctrl_flags flags = DMA_CTRL_ACK | DMA_PREP_INTERRUPT;
	dma_addr_t mapping;
	struct sk_buff *tmp;
	int i;

	struct sk_buff *skb;
	__u8 *ptr;
	dma_unmap_single(&piradio_p0.pdev->dev, piradio_p0.rx_dma.mapping,
			 MAX_BUF_SIZE, DMA_DEV_TO_MEM);

	pir = (struct piradio_hdr *)piradio_p0.rx_dma.rx_buffer;
	ip_header = (struct iphdr *)(piradio_p0.rx_dma.rx_buffer +
				     PIRADIO_HDR_LEN + ETH_HLEN);

	__u16 csum = ip_compute_csum((u8 *)piradio_p0.rx_dma.rx_buffer,
				     OFDM_FRAME_SIZE - 2);
	__u16 csum_rx = 0;
	memcpy(&csum_rx, piradio_p0.rx_dma.rx_buffer + OFDM_FRAME_SIZE - 2, 2);
	if (csum_rx != csum) {
		piradio_p0.link_stats.rx_dropped++;
		goto csum_error;
	}
	/* Compare ACKed sequence number with sequence number of frame in wait queue */
	if (pir->p_type == PIR_T_ACK) {
		piradio_p0.link_stats.rx_errors++;
		tmp = skb_peek(&wait_queue0);
		pir_tmp = (struct piradio_hdr *)tmp->data;
		if (ntohl(pir->p_seq_num) == htonl(pir_tmp->p_seq_num)) {
			ret = hrtimer_try_to_cancel(&piradio_p0.ack_timer);
			if (ret >= 0) {
				frame_acked0();
			}
		}
	} else if (pir->p_type == PIR_T_DATA) {
		if ((skb = netdev_alloc_skb(piradio_p0.netdev,
					    ntohs(ip_header->tot_len) +
						    ETH_HLEN + 2)) == NULL) {
			piradio_p0.link_stats.rx_dropped++;
			return;
		}
		if (ip_header->ihl == 5 && ip_header->version == 4) {
			if(piradio_p0.ack_enabled){

				ack_frame0(ntohl(pir->p_seq_num));

			}
			ip_header->daddr ^=
				0x00010000; /*Flip last bit of network address to fool the OS*/
			ip_header->saddr ^=
				0x00010000; /*Flip last bit of host address to fool the OS*/
			ip_header->check = 0;
			ip_header->check =
				ip_fast_csum((u8 *)ip_header, ip_header->ihl);
			skb_reserve(skb, 2);
			skb_put_data(skb,
				     piradio_p0.rx_dma.rx_buffer +
					     PIRADIO_HDR_LEN,
				     ntohs(ip_header->tot_len) + ETH_HLEN);
		} else {
			piradio_p0.link_stats.rx_dropped++;
			netdev_dbg(piradio_p0.netdev,"Dropping pkt");
			goto error;
		}

		skb->protocol = eth_type_trans(skb, piradio_p0.netdev);
		skb->ip_summed = CHECKSUM_UNNECESSARY;
		piradio_p0.link_stats.rx_packets++;
		piradio_p0.link_stats.rx_bytes += OFDM_FRAME_SIZE;

		netif_receive_skb(skb);

	}
	complete(completion);
	setup_rx(&piradio_p0);
	return;
csum_error:
	complete(completion);
	setup_rx(&piradio_p0);
	return;
error:
	complete(completion);
	setup_rx(&piradio_p0);
	kfree_skb(skb);
	return;
}

static void rx_cmplt_callback1(void *completion)
{
	ktime_t k;
	k = ktime_get();
	struct piradio_hdr *pir;
	struct piradio_hdr *pir_tmp;
	struct iphdr *ip_header;
	unsigned long l_flags;
	int ret;
	struct dma_async_tx_descriptor *chan_desc;
	enum dma_ctrl_flags flags = DMA_CTRL_ACK | DMA_PREP_INTERRUPT;
	dma_addr_t mapping;
	struct sk_buff *tmp;
	int len;
	struct sk_buff *skb;
	__u8 *ptr;
	dma_unmap_single(&piradio_p1.pdev->dev, piradio_p1.rx_dma.mapping,
			 MAX_BUF_SIZE, DMA_DEV_TO_MEM);

	pir = (struct piradio_hdr *)piradio_p1.rx_dma.rx_buffer;

	ip_header = (struct iphdr *)(piradio_p1.rx_dma.rx_buffer +
				     PIRADIO_HDR_LEN + ETH_HLEN);

	__u16 csum = ip_compute_csum((u8 *)piradio_p1.rx_dma.rx_buffer,
				     OFDM_FRAME_SIZE - 2);
	__u16 csum_rx = 0;
	memcpy(&csum_rx, piradio_p1.rx_dma.rx_buffer + OFDM_FRAME_SIZE - 2, 2);
	if (csum_rx != csum) {
		piradio_p1.link_stats.rx_dropped++;
		goto csum_error;
	}
	if (pir->p_type == PIR_T_ACK) {
		piradio_p1.link_stats.rx_errors++;

		tmp = skb_peek(&wait_queue1);
		pir_tmp = (struct piradio_hdr *)tmp->data;
		if (htonl(pir->p_seq_num) == htonl(pir_tmp->p_seq_num)) {
			ret = hrtimer_try_to_cancel(&piradio_p1.ack_timer);
			//printk(KERN_ALERT "p1: Timer canceled ret = %d", ret);
			if (ret >= 0) {
				frame_acked1();
			}
		}
	} else if (pir->p_type == PIR_T_DATA) {
		//endd1 = ktime_get();
		if ((skb = netdev_alloc_skb(piradio_p1.netdev,
					    ntohs(ip_header->tot_len) +
						    ETH_HLEN + 2)) == NULL) {
			piradio_p1.link_stats.rx_dropped++;
			return;
		}

		if (ip_header->ihl == 5 && ip_header->version == 4) {
			//printk(KERN_ALERT "p1: Will ACK");
			if(piradio_p1.ack_enabled)
				ack_frame1(htonl(pir->p_seq_num));
			ip_header->daddr ^=
				0x00010000; /*Flip last bit of network address to fool the OS*/
			ip_header->saddr ^=
				0x00010000; /*Flip last bit of host address to fool the OS*/
			ip_header->check = 0;

			ip_header->check =
				ip_fast_csum((u8 *)ip_header, ip_header->ihl);
		} else {
			piradio_p1.link_stats.rx_dropped++;
			netdev_dbg(piradio_p1.netdev,"Dropping pkt");
			goto error;
		}

		skb_reserve(skb, 2);
		skb_put_data(skb, piradio_p1.rx_dma.rx_buffer + PIRADIO_HDR_LEN,
			     ntohs(ip_header->tot_len) + ETH_HLEN);
		skb->protocol = eth_type_trans(skb, piradio_p1.netdev);
		skb->ip_summed = CHECKSUM_UNNECESSARY;
		piradio_p1.link_stats.rx_packets++;
		piradio_p1.link_stats.rx_bytes += OFDM_FRAME_SIZE;
		netif_receive_skb(skb);
	}

	complete(completion);
	setup_rx(&piradio_p1);
	return;
csum_error:
	complete(completion);
	setup_rx(&piradio_p1);
	return;
error:
	complete(completion);
	setup_rx(&piradio_p1);
	kfree_skb(skb);
	return;
}

int piradio_config(struct net_device *dev)
{
	return 0;
}

static void corr_cmplt_callback(void *completion)
{
	//printk(KERN_ALERT "Correlator config completed");
	complete(completion);
}

int piradio_prep_tx(struct sk_buff *skb, struct piradio_priv *piradio_p)
{
	unsigned long l_flags;
	int pad;
	int ret;
	struct dma_async_tx_descriptor *chan_desc;
	dma_async_tx_callback callback;
	__u32 csma_reg;
	enum dma_ctrl_flags flags = DMA_CTRL_ACK | DMA_PREP_INTERRUPT;
	dma_addr_t mapping;
	if (piradio_p == &piradio_p0) {
		callback = &tx_cmplt_callback0;
	} else if (piradio_p == &piradio_p1) {
		callback = &tx_cmplt_callback1;
	} else {
		printk(KERN_ALERT "prep_tx: netdevice not found");
		return -1;
	}

	ktime_t kt;
	__u32 backoff;
	__u16 ran;
	get_random_bytes(&ran, 2);
	__u16 csum = 0;
	csma_reg = read_reg(piradio_p->csma_delay_base);
	if (csma_reg > PIRADIO_CSMA_THRESHOLD) {
		get_random_bytes(&backoff, 4);
		kt = ktime_set(0, backoff % PIRADIO_CSMA_BACKOFF_MAX);
		//spin_lock_irqsave(&piradio_p->timer_lock, l_flags);
		hrtimer_start(&piradio_p->csma_timer, kt, HRTIMER_MODE_REL);
		//spin_unlock_irqrestore(&piradio_p->timer_lock, l_flags);
		return 0;
	}

	if (skb->len > 0) {
		spin_lock_irqsave(&piradio_p->lock, l_flags);
		if (skb->len % OFDM_FRAME_SIZE != 0) {
			pad = OFDM_FRAME_SIZE - (skb->len % OFDM_FRAME_SIZE);
			ret = skb_pad(skb, pad);
			get_random_bytes(skb->data + skb->tail, pad - 2);
			csum = ip_compute_csum((u8 *)skb->data,
					       OFDM_FRAME_SIZE - 2);
			memcpy(skb->data + OFDM_FRAME_SIZE - 2, &csum, 2);
			if (ret < 0) {
				netdev_dbg(piradio_p->netdev,"skb xpad error %d", ret);
				piradio_p->link_stats.tx_dropped++;
				goto error1;
			}
		}
		ret = config_dma(piradio_p->netdev, skb->data, skb->len + pad,
				 &mapping, DMA_MEM_TO_DEV, chan_desc, flags,
				 piradio_p->tx_dma.channel_p,
				 &piradio_p->pdev->dev, &piradio_p->tx_dma.cmp,
				 &piradio_p->tx_dma.cookie, callback);
		if (ret < 0) {
			piradio_p->link_stats.tx_dropped++;
			netdev_dbg(piradio_p->netdev,"prep_tx, dma config fail with %d", ret);
		}
		dma_unmap_single(&piradio_p->pdev->dev, mapping, MAX_BUF_SIZE,
				 DMA_DEV_TO_MEM);
		spin_unlock_irqrestore(&piradio_p->lock, l_flags);
	}
	return 0;
error1: //spin_unlock_irqrestore(&piradio_p->lock, l_flags);
	return -1;
}

static enum hrtimer_restart csma_timer_handler0(struct hrtimer *timer)
{
	struct sk_buff *tmp;
	tmp = skb_peek(&wait_queue0);
	piradio_prep_tx(tmp, &piradio_p0);
	return HRTIMER_NORESTART;
}

static enum hrtimer_restart csma_timer_handler1(struct hrtimer *timer)
{
	struct sk_buff *tmp;
	tmp = skb_peek(&wait_queue1);
	piradio_prep_tx(tmp, &piradio_p1);
	return HRTIMER_NORESTART;
}

static enum hrtimer_restart timer_handler(struct hrtimer *timer)
{
	hrtimer_forward(timer, timer->base->get_time(), ktime_set(10, 0));
	printk(KERN_ALERT
	       "wait0 = %llu pending0 = %llu wait1 = %llu pending1 = %llu is stopped0 = %d, is stopped1 = %d",
	       skb_queue_len(&wait_queue0), skb_queue_len(&pending_queue0),
	       skb_queue_len(&wait_queue1), skb_queue_len(&pending_queue1),
	       netif_queue_stopped(piradio_p0.netdev),
	       netif_queue_stopped(piradio_p1.netdev));
	return HRTIMER_RESTART;
}

static enum hrtimer_restart ack_timer_handler0(struct hrtimer *timer)
{
	//printk(KERN_ALERT "ACK timer 0");
	piradio_p0.link_stats.retransmissions++;
	hrtimer_start(&piradio_p0.csma_timer, 0, HRTIMER_MODE_REL);
	return HRTIMER_NORESTART;
}

static enum hrtimer_restart ack_timer_handler1(struct hrtimer *timer)
{
	//printk(KERN_ALERT "ACK timer 1");
	piradio_p1.link_stats.retransmissions++;
	hrtimer_start(&piradio_p1.csma_timer, 0, HRTIMER_MODE_REL);
	return HRTIMER_NORESTART;
}

netdev_tx_t piradio_tx(struct sk_buff *skb, struct net_device *dev)
{
	int ret;
	int start = 0;
	struct sk_buff *buf_ptr;
	struct sk_buff sk;
	struct piradio_priv *piradio_p;
	unsigned long l_flags;
	struct piradio_hdr *pir = skb_push(skb, PIRADIO_HDR_LEN);

	pir->p_type = PIR_T_DATA;

	if (dev == piradio_p0.netdev) {
		piradio_p = &piradio_p0;
		pir->p_seq_num = htonl(piradio_p->sequence_num++);
		piradio_p->csma_timer.function = csma_timer_handler0;
		spin_lock_irqsave(&piradio_p0.timer_lock, l_flags);
		if (skb_queue_empty(&wait_queue0) &&
		    skb_queue_empty(&pending_queue0)) {
			//printk(KERN_ALERT "TX all empty");
			skb_queue_head(&wait_queue0, skb);
			//wake_up_interruptible(&tx0_waitqueue);
			hrtimer_start(&piradio_p->csma_timer, 0,
				      HRTIMER_MODE_REL);
			spin_unlock_irqrestore(&piradio_p0.timer_lock, l_flags);
		} else {
			if (skb_queue_len(&pending_queue0) >=
			    MAX_PENDING_Q_LEN) {
				spin_unlock_irqrestore(&piradio_p0.timer_lock,
						       l_flags);
				piradio_p->link_stats.tx_dropped++;
				goto tx_error;
			}
			skb_queue_tail(&pending_queue0, skb);

			if (skb_queue_len(&pending_queue0) ==
			    MAX_PENDING_Q_LEN) {
				netif_stop_queue(dev);
				//printk(KERN_ALERT "STOP QUEUE 0");
			}
			spin_unlock_irqrestore(&piradio_p0.timer_lock, l_flags);
		}
	} else if (dev == piradio_p1.netdev) {
		piradio_p = &piradio_p1;
		pir->p_seq_num = htonl(piradio_p->sequence_num++);
		piradio_p->csma_timer.function = csma_timer_handler1;
		spin_lock_irqsave(&piradio_p1.timer_lock, l_flags);
		if (skb_queue_empty(&wait_queue1) &&
		    skb_queue_empty(&pending_queue1)) {
			skb_queue_head(&wait_queue1, skb);
			//spin_lock_irqsave(&piradio_p1.timer_lock, l_flags);
			//wake_up_interruptible(&tx1_waitqueue);
			hrtimer_start(&piradio_p->csma_timer, 0,
				      HRTIMER_MODE_REL);
			spin_unlock_irqrestore(&piradio_p1.timer_lock, l_flags);
		} else {
			if (skb_queue_len(&pending_queue1) >=
			    MAX_PENDING_Q_LEN) {
				spin_unlock_irqrestore(&piradio_p1.timer_lock,
						       l_flags);
				piradio_p->link_stats.tx_dropped++;
				goto tx_error;
			}
			skb_queue_tail(&pending_queue1, skb);

			if (skb_queue_len(&pending_queue1) ==
			    MAX_PENDING_Q_LEN) {
				netif_stop_queue(dev);
				//printk(KERN_ALERT "STOP QUEUE 0");
			}
			spin_unlock_irqrestore(&piradio_p1.timer_lock, l_flags);
		}
	} else {
		printk(KERN_ALERT "prep_tx: netdevice not found");
		goto tx_error;
	}
	//dev_consume_skb_any(skb);
	return 0;
tx_error:
	//dev_consume_skb_any(skb);
	return -1;
}

static int piradio_ioctl(struct net_device *dev, struct ifreq *rq, int cmd)
{
	struct piradio_priv *piradio_p;
	if (dev == piradio_p0.netdev)
		piradio_p = &piradio_p0;
	else if (dev == piradio_p1.netdev)
		piradio_p = &piradio_p1;
	else {
		printk(KERN_ALERT "ioctl: netdevice not found");
		return -1;
	}
	struct dma_async_tx_descriptor *chan_desc;
	enum dma_ctrl_flags flags = DMA_CTRL_ACK | DMA_PREP_INTERRUPT;
	dma_addr_t mapping;
	unsigned long l_flags;
	int ret;
	int i;
	__u32 *taps;

	struct config_data *conf = (struct config_data *)rq->ifr_ifru.ifru_data;
	//printk(KERN_ALERT "IOCTL called\n");
	switch (cmd) {
	case PIRADIO_RESET_MOD:
		write_reg(conf->length, piradio_p->modul_base);
		return 0;
	case PIRADIO_CONFIG_CORRELATOR:
		if (conf->length) {
			spin_lock_irqsave(&piradio_p->lock, l_flags);
			ret = config_dma(NULL, (unsigned char *)conf->data,
					 conf->length, &mapping, DMA_MEM_TO_DEV,
					 chan_desc, flags,
					 piradio_p->correlator_dma.channel_p,
					 &piradio_p->pdev->dev,
					 &piradio_p->correlator_dma.cmp,
					 &piradio_p->correlator_dma.cookie,
					 &corr_cmplt_callback);

			ret = config_dma(NULL, (unsigned char *)conf->data,
					 conf->length, &mapping, DMA_MEM_TO_DEV,
					 chan_desc, flags,
					 piradio_p->correlator2_dma.channel_p,
					 &piradio_p->pdev->dev,
					 &piradio_p->correlator2_dma.cmp,
					 &piradio_p->correlator2_dma.cookie,
					 &corr_cmplt_callback);
			spin_unlock_irqrestore(&piradio_p->lock, l_flags);
		}
		if (ret == 0) {
			//printk(KERN_ALERT "Correlator config OK\n");
			return 0;
		} else {
			return -1;
		}
	case PIRADIO_CONFIG_FRAMER:
		if (conf->length) {
			spin_lock_irqsave(&piradio_p->lock, l_flags);
			ret = config_dma(NULL, (unsigned char *)conf->data,
					 conf->length, &mapping, DMA_MEM_TO_DEV,
					 chan_desc, flags,
					 piradio_p->framer_config_dma.channel_p,
					 &piradio_p->pdev->dev,
					 &piradio_p->framer_config_dma.cmp,
					 &piradio_p->framer_config_dma.cookie,
					 &corr_cmplt_callback);
			spin_unlock_irqrestore(&piradio_p->lock, l_flags);
		}
		if (ret == 0) {
			//printk(KERN_ALERT "Framer config OK\n");
			return 0;
		} else {
			return -1;
		}
	case PIRADIO_CONFIG_FFT_TX:
		if (conf->length) {
			spin_lock_irqsave(&piradio_p->lock, l_flags);
			ret = config_dma(NULL, (unsigned char *)conf->data,
					 conf->length, &mapping, DMA_MEM_TO_DEV,
					 chan_desc, flags,
					 piradio_p->fft_tx_dma.channel_p,
					 &piradio_p->pdev->dev,
					 &piradio_p->fft_tx_dma.cmp,
					 &piradio_p->fft_tx_dma.cookie,
					 &corr_cmplt_callback);
			spin_unlock_irqrestore(&piradio_p->lock, l_flags);
		}
		if (ret == 0) {
			//printk(KERN_ALERT "FFT TX config OK\n");
			return 0;
		} else {
			return -1;
		}
	case PIRADIO_CONFIG_FFT_RX:
		if (conf->length) {
			spin_lock_irqsave(&piradio_p->lock, l_flags);
			ret = config_dma(NULL, (unsigned char *)conf->data,
					 conf->length, &mapping, DMA_MEM_TO_DEV,
					 chan_desc, flags,
					 piradio_p->fft_rx_dma.channel_p,
					 &piradio_p->pdev->dev,
					 &piradio_p->fft_rx_dma.cmp,
					 &piradio_p->fft_rx_dma.cookie,
					 &corr_cmplt_callback);
			ret = config_dma(NULL, (unsigned char *)conf->data,
					 conf->length, &mapping, DMA_MEM_TO_DEV,
					 chan_desc, flags,
					 piradio_p->fft_rx2_dma.channel_p,
					 &piradio_p->pdev->dev,
					 &piradio_p->fft_rx2_dma.cmp,
					 &piradio_p->fft_rx2_dma.cookie,
					 &corr_cmplt_callback);

			spin_unlock_irqrestore(&piradio_p->lock, l_flags);
		}
		if (ret == 0) {
			///printk(KERN_ALERT "FFT RX config OK\n");
			return 0;
		} else {
			return -1;
		}
	case PIRADIO_CONFIG_FIR:
//		taps = (__u32 *)conf->data;
//		if (conf->length) {
//			for (i = 0; i < conf->length / 4; i++) {
//				write_reg(taps[i], piradio_p->fir_filt_base +
//							   i * TAP_SIZE);
//			}
//		}
		return 0;
	case PIRADIO_CONFIG_FREQ_OFF:
		write_reg(conf->length, piradio_p->chann_emm_base);
		return 0;
	case PIRADIO_CONFIG_STHRESH:
		write_reg(80000, piradio_p->sync_thresh_base);
		return 0;
	case PIRADIO_CONFIG_CSMA:
		if (conf->length) {
			write_reg(conf->length, piradio_p->csma_delay_base);
		}
		break;
	case PIRADIO_CONFIG_ACK:
		printk(KERN_ALERT "Ack enabled %d", conf->length);
		if (conf->length) {
			piradio_p->ack_enabled = 1;
		}
		else{
			piradio_p->ack_enabled = 0;
		}
		break;
	}
	return 0;
}

static void piradio_stats(struct net_device *dev, struct rtnl_link_stats64 *stats)
{
	struct piradio_priv *piradio_p;
	if (dev == piradio_p0.netdev)
		piradio_p = &piradio_p0;
	else if (dev == piradio_p1.netdev)
		piradio_p = &piradio_p1;

	stats->collisions = piradio_p->link_stats.retransmissions;
	stats->tx_packets = piradio_p->link_stats.tx_packets;
	stats->tx_bytes = piradio_p->link_stats.tx_bytes;
	stats->tx_errors = piradio_p->link_stats.tx_errors;
	stats->tx_dropped = piradio_p->link_stats.tx_dropped;
	stats->rx_bytes = piradio_p->link_stats.rx_bytes;
	stats->rx_errors = piradio_p->link_stats.rx_errors;
	stats->rx_packets = piradio_p->link_stats.rx_packets;
	stats->rx_dropped = piradio_p->link_stats.rx_dropped;
}

int piradio_rebuild_header(struct net_device *dev)
{
	return 0;
}

int piradio_set_mac(struct net_device *dev, void *addr)
{
	return 0;
}

int piradio_dev_init(struct net_device *dev)
{
	return 0;
}

int piradio_open(struct net_device *dev)
{
	printk(KERN_ALERT "Open!!!!!");
	enum dma_ctrl_flags flags = DMA_CTRL_ACK | DMA_PREP_INTERRUPT;
	int i, ret;
	hrtimer_init(&timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
	timer.function = timer_handler;

	netif_start_queue(dev);
	struct piradio_priv *piradio_p;
	if (dev == piradio_p0.netdev) {
		piradio_p = &piradio_p0;
		memcpy(dev->dev_addr, "PIRAD0", PIRADIO_HW_ALEN);
		hrtimer_init(&piradio_p->csma_timer, CLOCK_MONOTONIC,
			     HRTIMER_MODE_REL);
		piradio_p->csma_timer.function = csma_timer_handler0;

		hrtimer_init(&piradio_p->ack_timer, CLOCK_MONOTONIC,
			     HRTIMER_MODE_REL);
		piradio_p->ack_timer.function = ack_timer_handler0;
		printk(KERN_ALERT "PIRADIO0 OPEN");
	} else if (dev == piradio_p1.netdev) {
		piradio_p = &piradio_p1;
		memcpy(dev->dev_addr, "PIRAD1", PIRADIO_HW_ALEN);
		hrtimer_init(&piradio_p->csma_timer, CLOCK_MONOTONIC,
			     HRTIMER_MODE_REL);
		piradio_p->csma_timer.function = csma_timer_handler1;

		hrtimer_init(&piradio_p->ack_timer, CLOCK_MONOTONIC,
			     HRTIMER_MODE_REL);
		piradio_p->ack_timer.function = ack_timer_handler1;
		printk(KERN_ALERT "PIRADIO1 OPEN");
	} else {
		printk(KERN_ALERT "open: netdevice not found");
		return -1;
	}

	piradio_p->rx_dma.rx_buffer = kmalloc(MAX_BUF_SIZE, GFP_KERNEL);

	piradio_p->link_stats.retransmissions = 0;
	piradio_p->link_stats.rx_bytes = 0;
	piradio_p->link_stats.rx_dropped = 0;
	piradio_p->link_stats.rx_errors = 0;
	piradio_p->link_stats.rx_packets = 0;
	piradio_p->link_stats.tx_bytes = 0;
	piradio_p->link_stats.tx_dropped = 0;
	piradio_p->link_stats.tx_errors = 0;
	piradio_p->link_stats.tx_packets = 0;
	piradio_p->tx_tries = 0;
	piradio_p->csma_tries = 0;
	piradio_p->sequence_num = 0;
	piradio_p->ack_enabled = 1;
	skb_queue_head_init(&wait_queue0);
	skb_queue_head_init(&sent_queue0);
	skb_queue_head_init(&pending_queue0);
	skb_queue_head_init(&wait_queue1);
	skb_queue_head_init(&sent_queue1);
	skb_queue_head_init(&pending_queue1);

	ack_buffer0 = kmalloc(MAX_BUF_SIZE, GFP_KERNEL);
	ack_buffer1 = kmalloc(MAX_BUF_SIZE, GFP_KERNEL);

	get_random_bytes(ack_buffer0 + sizeof(struct piradio_hdr),
			 OFDM_FRAME_SIZE - sizeof(struct piradio_hdr));
	get_random_bytes(ack_buffer1 + sizeof(struct piradio_hdr),
			 OFDM_FRAME_SIZE - sizeof(struct piradio_hdr));

	setup_rx(piradio_p);

	return 0;
}

static int piradio_down(struct net_device *dev)
{
	int i;
	struct piradio_priv *piradio_p;
	if (dev == piradio_p0.netdev) {
		piradio_p = &piradio_p0;
		printk(KERN_ALERT "PIRADIO0 DOWN");
		skb_queue_purge(&wait_queue0);
		skb_queue_purge(&pending_queue0);
		skb_queue_purge(&sent_queue0);
	} else if (dev == piradio_p1.netdev) {
		piradio_p = &piradio_p1;
		printk(KERN_ALERT "PIRADIO1 DOWN");
		skb_queue_purge(&wait_queue1);
		skb_queue_purge(&pending_queue1);
		skb_queue_purge(&sent_queue1);
	} else {
		printk(KERN_ALERT "down: netdevice not found");
		return -1;
	}

	kfree(piradio_p->rx_dma.rx_buffer);
	curr_desc = 0;
	//kfree(piradio_p->ack_buffer);
	netif_stop_queue(dev);
	return 0;
}

static const struct net_device_ops piradio_ops = {
	.ndo_open = piradio_open,
	.ndo_stop = piradio_down,
	.ndo_init = piradio_dev_init,
	.ndo_start_xmit = piradio_tx,
	.ndo_do_ioctl = piradio_ioctl,
	.ndo_get_stats64 = piradio_stats,
};

void ret_0(struct net_device *dev)
{
	return;
}

int piradio_header(struct sk_buff *skb, struct net_device *dev,
		   unsigned short type, const void *daddr, const void *saddr,
		   unsigned int len)
{
	struct ethhdr *eth = skb_push(skb, ETH_HLEN);
	if (type != ETH_P_802_3 && type != ETH_P_802_2)
		eth->h_proto = htons(type);
	else
		eth->h_proto = htons(len);

	if (!saddr)
		saddr = dev->dev_addr;
	memcpy(eth->h_source, saddr, ETH_ALEN);

	if (daddr) {
		if (dev == piradio_p0.netdev)
			memcpy(eth->h_dest, piradio_p1.netdev->dev_addr,
			       PIRADIO_HW_ALEN);
		else if (dev == piradio_p1.netdev)
			memcpy(eth->h_dest, piradio_p0.netdev->dev_addr,
			       PIRADIO_HW_ALEN);
	}
	return ETH_HLEN;
}

//static const struct ethtool_ops piradio_ethtool_ops = {
//	.get_link		= ret_0,
//	.get_ts_info		= ret_0,
//};

const struct header_ops header_ops ____cacheline_aligned = {
	.create = piradio_header,
	//	.parse		= ret_0,
	//	.cache		= ret_0,
	//	.cache_update	= ret_0,
	//	.parse_protocol	= ret_0,
};

void piradio_setup(struct net_device *dev)
{
	ether_setup(dev);
	dev->mtu = 1000;
	dev->hard_header_len = PIRADIO_HDR_LEN + ETH_HLEN; /* 14	*/
	dev->min_header_len = PIRADIO_HDR_LEN + ETH_HLEN; /* 14	*/
	dev->addr_len = PIRADIO_HW_ALEN; /* 6	*/
	dev->flags = IFF_NOARP | IFF_DEBUG | IFF_BROADCAST | IFF_MULTICAST;
	//	dev->priv_flags |= IFF_LIVE_ADDR_CHANGE | IFF_NO_QUEUE ;
	dev->priv_flags |= IFF_TX_SKB_SHARING;
	//	netif_keep_dst(dev);
	//	dev->hw_features = NETIF_F_GSO_SOFTWARE;
	//	dev->features = NETIF_F_SG | NETIF_F_FRAGLIST | NETIF_F_GSO_SOFTWARE |
	//			NETIF_F_NO_CSUM | NETIF_F_RXCSUM | NETIF_F_SCTP_CRC |
	//			NETIF_F_HIGHDMA | NETIF_F_LLTX | NETIF_F_NETNS_LOCAL |
	//			NETIF_F_VLAN_CHALLENGED;
	//	dev->ethtool_ops	= &piradio_ethtool_ops;
	dev->header_ops = &header_ops;
	dev->netdev_ops = &piradio_ops;
	dev->needs_free_netdev = 1;
	dev->priv_destructor = ret_0;
}

static int piradio_probe(struct platform_device *pdev)
{
	int rc;
	int err;
	remap_devs();

	write_reg(0x00000000U, reset_base);
	udelay(100);
	write_reg(0x80000000U, reset_base);

	//printk(KERN_INFO "dma_proxy module initialized\n");
	printk(KERN_INFO "piradio module trying init...\n");

	piradio_p0.tx_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio0_tx");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p0.tx_dma.channel_p));
	if (IS_ERR(piradio_p0.tx_dma.channel_p)) {
		err = PTR_ERR(piradio_p0.tx_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio0: No Tx channel\n");
		return err;
	}

	piradio_p0.rx_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio0_rx");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p0.rx_dma.channel_p));
	if (IS_ERR(piradio_p0.rx_dma.channel_p)) {
		err = PTR_ERR(piradio_p0.rx_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio0: No Rx channel\n");
		return err;
	}

	piradio_p0.framer_config_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio0_fc");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p0.framer_config_dma.channel_p));
	if (IS_ERR(piradio_p0.framer_config_dma.channel_p)) {
		err = PTR_ERR(piradio_p0.framer_config_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio0: No Framer Config channel\n");
		return err;
	}

	piradio_p0.fft_tx_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio0_fft_tx");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p0.fft_tx_dma.channel_p));
	if (IS_ERR(piradio_p0.fft_tx_dma.channel_p)) {
		err = PTR_ERR(piradio_p0.fft_tx_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio0: No FFT TX channel\n");
		return err;
	}

	piradio_p0.correlator_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio0_corr");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p0.correlator_dma.channel_p));
	if (IS_ERR(piradio_p0.correlator_dma.channel_p)) {
		err = PTR_ERR(piradio_p0.correlator_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio0: No Correlator channel\n");
		return err;
	}

	piradio_p0.correlator2_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio0_corrr");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p0.correlator2_dma.channel_p));
	if (IS_ERR(piradio_p0.correlator2_dma.channel_p)) {
		err = PTR_ERR(piradio_p0.correlator2_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio0: No Correlator2 channel\n");
		return err;
	}

	piradio_p0.fft_rx_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio0_fft_rx");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p0.fft_rx_dma.channel_p));
	if (IS_ERR(piradio_p0.fft_rx_dma.channel_p)) {
		err = PTR_ERR(piradio_p0.fft_rx_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio0: No FFT RX channel\n");
		return err;
	}

	piradio_p0.fft_rx2_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio0_fft_rx2");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p0.fft_rx2_dma.channel_p));
	if (IS_ERR(piradio_p0.fft_rx2_dma.channel_p)) {
		err = PTR_ERR(piradio_p0.fft_rx2_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio0: No FFT RX channel\n");
		return err;
	}

	piradio_p0.pdev = pdev;

	piradio_p1.tx_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio1_tx");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p1.tx_dma.channel_p));
	if (IS_ERR(piradio_p1.tx_dma.channel_p)) {
		err = PTR_ERR(piradio_p1.tx_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio1: No Tx channel\n");
		return err;
	}

	piradio_p1.rx_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio1_rx");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p1.rx_dma.channel_p));
	if (IS_ERR(piradio_p1.rx_dma.channel_p)) {
		err = PTR_ERR(piradio_p1.rx_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio1: No Rx channel\n");
		return err;
	}

	piradio_p1.framer_config_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio1_fc");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p1.framer_config_dma.channel_p));
	if (IS_ERR(piradio_p1.framer_config_dma.channel_p)) {
		err = PTR_ERR(piradio_p1.framer_config_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio1: No Framer Config channel\n");
		return err;
	}

	piradio_p1.fft_tx_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio1_fft_tx");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p1.fft_tx_dma.channel_p));
	if (IS_ERR(piradio_p1.fft_tx_dma.channel_p)) {
		err = PTR_ERR(piradio_p1.fft_tx_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio1: No FFT TX channel\n");
		return err;
	}

	piradio_p1.correlator_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio1_corr");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p1.correlator_dma.channel_p));
	if (IS_ERR(piradio_p1.correlator_dma.channel_p)) {
		err = PTR_ERR(piradio_p1.correlator_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio1: No Correlator channel\n");
		return err;
	}

	piradio_p1.correlator2_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio1_corr2");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p1.correlator2_dma.channel_p));
	if (IS_ERR(piradio_p1.correlator2_dma.channel_p)) {
		err = PTR_ERR(piradio_p1.correlator2_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio1: No Correlator channel\n");
		return err;
	}
	piradio_p1.fft_rx_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio1_fft_rx");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p1.fft_rx_dma.channel_p));
	if (IS_ERR(piradio_p1.fft_rx_dma.channel_p)) {
		err = PTR_ERR(piradio_p1.fft_rx_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio1: No FFT RX channel\n");
		return err;
	}

	piradio_p1.fft_rx2_dma.channel_p =
		dma_request_chan(&pdev->dev, "piradio1_fft_rx2");
	printk(KERN_ALERT "Chann request returned %d \n",
	       PTR_ERR(piradio_p1.fft_rx2_dma.channel_p));
	if (IS_ERR(piradio_p1.fft_rx2_dma.channel_p)) {
		err = PTR_ERR(piradio_p1.fft_rx2_dma.channel_p);
		if (err != -EPROBE_DEFER)
			pr_err("piradio1: No FFT RX channel\n");
		return err;
	}
	piradio_p1.pdev = pdev;

	printk(KERN_INFO "dma_proxy module init ok\n");
	return 0;
}

static int piradio_remove(struct platform_device *pdev)
{
	return 0;
}

static const struct of_device_id piradio_of_ids[] = {
	{
		.compatible = "xlnx,piradio",
	},
	{}
};

static struct platform_driver piradio_driver =
  { .driver =
    { .name = "piradio_driver", .owner = THIS_MODULE, .of_match_table =
	piradio_of_ids, }, .probe = piradio_probe, .remove = piradio_remove, };

static int __init piradio_init(void)
{
	printk(KERN_ALERT "Hello World\n");

	int err;

	err = -ENOMEM;
	piradio_p0.netdev =
		alloc_netdev(0, "piradio%d", NET_NAME_UNKNOWN, piradio_setup);
	err = register_netdev(piradio_p0.netdev);
	if (err)
		goto out_free_netdev;

	piradio_p1.netdev =
		alloc_netdev(0, "piradio%d", NET_NAME_UNKNOWN, piradio_setup);
	err = register_netdev(piradio_p1.netdev);
	if (err)
		goto out_free_netdev;

	return platform_driver_register(&piradio_driver);
out_free_netdev:
	free_netdev(piradio_p0.netdev);
	free_netdev(piradio_p1.netdev);
	return -1;
}

static void piradio_exit(void)
{
	dmaengine_terminate_async(piradio_p0.rx_dma.channel_p);
	dmaengine_synchronize(piradio_p0.rx_dma.channel_p);
	dma_release_channel(piradio_p0.rx_dma.channel_p);
	dmaengine_terminate_async(piradio_p0.tx_dma.channel_p);
	dma_release_channel(piradio_p0.tx_dma.channel_p);
	dmaengine_terminate_async(piradio_p0.correlator_dma.channel_p);
	dma_release_channel(piradio_p0.correlator_dma.channel_p);
	dmaengine_terminate_async(piradio_p0.correlator2_dma.channel_p);
	dma_release_channel(piradio_p0.correlator2_dma.channel_p);
	dmaengine_terminate_async(piradio_p0.fft_rx_dma.channel_p);
	dma_release_channel(piradio_p0.fft_rx_dma.channel_p);
	dmaengine_terminate_async(piradio_p0.fft_rx2_dma.channel_p);
	dma_release_channel(piradio_p0.fft_rx2_dma.channel_p);
	dmaengine_terminate_async(piradio_p0.fft_tx_dma.channel_p);
	dma_release_channel(piradio_p0.fft_tx_dma.channel_p);
	dmaengine_terminate_async(piradio_p0.framer_config_dma.channel_p);
	dma_release_channel(piradio_p0.framer_config_dma.channel_p);

	dmaengine_terminate_async(piradio_p1.rx_dma.channel_p);
	dmaengine_synchronize(piradio_p1.rx_dma.channel_p);
	dma_release_channel(piradio_p1.rx_dma.channel_p);
	dmaengine_terminate_async(piradio_p1.tx_dma.channel_p);
	dma_release_channel(piradio_p1.tx_dma.channel_p);
	dmaengine_terminate_async(piradio_p1.correlator_dma.channel_p);
	dma_release_channel(piradio_p1.correlator_dma.channel_p);
	dmaengine_terminate_async(piradio_p1.correlator2_dma.channel_p);
	dma_release_channel(piradio_p1.correlator2_dma.channel_p);
	dmaengine_terminate_async(piradio_p1.fft_rx_dma.channel_p);
	dma_release_channel(piradio_p1.fft_rx_dma.channel_p);
	dmaengine_terminate_async(piradio_p1.fft_rx2_dma.channel_p);
	dma_release_channel(piradio_p1.fft_rx2_dma.channel_p);
	dmaengine_terminate_async(piradio_p1.fft_tx_dma.channel_p);
	dma_release_channel(piradio_p1.fft_tx_dma.channel_p);
	dmaengine_terminate_async(piradio_p1.framer_config_dma.channel_p);
	dma_release_channel(piradio_p1.framer_config_dma.channel_p);

	unregister_netdev(piradio_p0.netdev);
	unregister_netdev(piradio_p1.netdev);
	unmap_devs();

	platform_driver_unregister(&piradio_driver);
	//	free_netdev(dev);
}

module_init(piradio_init);
module_exit(piradio_exit);
